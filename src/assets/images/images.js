import logo from './logo.svg';
import logo_footer from './logo_footer.svg';
import main_background from './main_background@3x.jpg';
import main_background_mobile from './main_background_mobile@3x.png';
import plan_background from './plan_background@3x.png';
import plan_background_mobile from './plan_background_mobile@3x.png';
import tiles_background from './tiles_background@3x.png';
import tiles_background_mobile from './tiles_background_mobile@3x.png';
import platform from './platform@3x.png';

import logo1 from './logos/logo1@3x.png';
import logo2 from './logos/logo2@3x.png';
import logo3 from './logos/logo3@3x.png';
import logo4 from './logos/logo4@3x.png';
import logo5 from './logos/logo5@3x.png';

// Icons
import social_facebook from './social/logo_facebook.svg';
import social_twitter from './social/logo_twitter.svg';
import included from './icons/included.svg';
import included_white from './icons/included_white.svg';
import chevron_open from './icons/chevron_open.svg';
import chevron_close from './icons/chevron_close.svg';

// SubModules
import sub_module_1 from './sub_module_1.png';
import sub_module_2 from './sub_module_2.png';
import sub_module_3 from './sub_module_3.png';
import sub_module_4 from './sub_module_4.png';
import sub_module_5 from './sub_module_5.png';

const images = {
  logo: logo,
  logo_footer: logo_footer,
  main_background: main_background,
  main_background_mobile: main_background_mobile,
  plan_background: plan_background,
  plan_background_mobile: plan_background_mobile,
  tiles_background: tiles_background,
  tiles_background_mobile: tiles_background_mobile,
  platform: platform,
  logo1: logo1,
  logo2: logo2,
  logo3: logo3,
  logo4: logo4,
  logo5: logo5,
  sub_module_1: sub_module_1,
  sub_module_2: sub_module_2,
  sub_module_3: sub_module_3,
  sub_module_4: sub_module_4,
  sub_module_5: sub_module_5
};

const icons = {
  social_facebook: social_facebook,
  social_twitter: social_twitter,

  chevron_open: chevron_open,
  chevron_close: chevron_close,
  included: included,
  included_white: included_white
};

export default { images, icons };
