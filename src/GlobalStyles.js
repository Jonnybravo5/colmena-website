import { createGlobalStyle } from 'styled-components';

const size = {
  phoneSmall: '20em',
  phoneMedium: '23em',
  phonePlus: '26em',
  tablet: '48.1em',
  desktopSmall: '64em',
  desktopMedium: '80em',
  desktopLarge: '90em',
  desktopHd: '120em',
  desktop4k: '240em'
};

export const device = {
  belowPhoneSmall: `(max-width: ${size.phoneSmall})`,
  belowPhoneMedium: `(max-width: ${size.phoneMedium})`,
  belowPhonePlus: `(max-width: ${size.phonePlus})`,
  belowTablet: `(max-width: ${size.tablet})`,
  belowDesktopSmall: `(max-width: ${size.desktopSmall})`,
  belowDesktopMedium: `(max-width: ${size.desktopMedium})`,
  belowDesktopLarge: `(max-width: ${size.desktopLarge})`,
  belowDesktopHd: `(max-width: ${size.desktopHd})`,
  belowDesktop4k: `(max-width: ${size.desktop4k})`,

  abovePhoneSmall: `(min-width: ${size.phoneSmall})`,
  abovePhoneMedium: `(min-width: ${size.phoneMedium})`,
  abovePhonePlus: `(min-width: ${size.phonePlus})`,
  aboveTablet: `(min-width: ${size.tablet})`,
  aboveDesktopSmall: `(min-width: ${size.desktopSmall})`,
  aboveDesktopMedium: `(min-width: ${size.desktopMedium})`,
  aboveDesktopLarge: `(min-width: ${size.desktopLarge})`,
  aboveDesktopHd: `(min-width: ${size.desktopHd})`,
  aboveDesktop4k: `(min-width: ${size.desktop4k})`
};

export const theme = {
  primary: '#6600ff',
  grey: '#afafaf',
  background: '#ffffff',
  backgroundGrey: '#fbfbfb',
  textColor: '#191919'
};

export const GlobalStyle = createGlobalStyle`
  html {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    font-size: 16px;
    @media ${device.belowTablet} {
      font-size: 10px;
    }
  }


  body {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    margin: 0;
    padding: 0;
    font-family: 'Catamaran', sans-serif;
    background-color: rgba(222, 224, 233, 0.01);
    line-height: 28px;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  h1 {
    font-family: Catamaran;
    font-weight: 900;
    line-height: 1.03;
    font-size: 4.250em;
    color: ${props => props.theme.textColor};
    margin: 0;
    padding: 25px 0;
    box-sizing: border-box;
  }

  h2 {
    font-family: Catamaran;
    font-size: 3.425em;
    font-weight: 900;
    color: ${props => props.theme.textColor};
    line-height: 1.14;
    margin: 0;
    padding: 20px 0;
    box-sizing: border-box;
  }

  h3 {
    font-family: Catamaran;
    font-weight: 500;
    font-size: 1.525em;
    color: ${props => props.theme.textColor};
    line-height: 1.38;
    margin: 0;
    padding: 10px 0;
    box-sizing: border-box;
  }

  a{
    text-decoration: none;
  }

  p, ol {
    margin: 0;
  }

  .blue-text {
    color: ${props => props.theme.primary};
  }
`;
