import assets from "./assets/images/images";

export const { images, icons } = assets;

export const sizes = {
  header: {
    height: "78px",
    sidePadding: "135px",
    logoWidth: "155px",
    navigationPaddingLeft: "85px"
  },

  faq: {
    sidePadding: "135px"
  },

  footer: {
    height: "95px",
    logoWidth: "118px",
    sidePadding: "135px"
  }
};
