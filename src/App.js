import React, { Component } from 'react';
import { ThemeProvider } from 'styled-components';

import { theme, GlobalStyle } from './GlobalStyles';
import { images } from './GlobalVariables';

import Layout from './containers/Layout/Layout';
import MainModule from './containers/MainModule/MainModule';
import HeroContent from './containers/MainModule/HeroContent/HeroContent';
import PlanContent from './containers/MainModule/PlanContent/PlanContent';
import SubModule from './containers/SubModule/SubModule';
import FAQContent from './containers/MainModule/FAQContent/FAQContent';

const background = {
  hero: {
    mobile: images.main_background_mobile,
    desktop: images.main_background
  },
  plan: {
    mobile: images.plan_background_mobile,
    desktop: images.plan_background
  },
  tiles: {
    mobile: images.tiles_background_mobile,
    desktop: images.tiles_background
  }
};
class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <GlobalStyle />
          <Layout>
            <MainModule
              title='hero.title'
              text='hero.text'
              background={background.hero}
            >
              <HeroContent />
            </MainModule>
            <SubModule
              category='subModule1.category'
              title='subModule1.title'
              text='subModule1.text'
              buttonText='subModule1.buttonText'
              image={images.sub_module_1}
            ></SubModule>
            <SubModule
              category='subModule2.category'
              title='subModule2.title'
              text='subModule2.text'
              buttonText='subModule2.buttonText'
              image={images.sub_module_2}
              background='white'
              imagePos='right'
            ></SubModule>
            <SubModule
              category='subModule3.category'
              title='subModule3.title'
              text='subModule3.text'
              buttonText='subModule3.buttonText'
              image={images.sub_module_3}
            ></SubModule>
            <SubModule
              category='subModule4.category'
              title='subModule4.title'
              text='subModule4.text'
              buttonText='subModule4.buttonText'
              image={images.sub_module_4}
              background='white'
              imagePos='right'
            ></SubModule>
            <MainModule
              title='faq.title'
              text='faq.text'
              background={background.plan}
            >
              <PlanContent />
            </MainModule>
            <SubModule
              category='subModule5.category'
              title='subModule5.title'
              text='subModule5.text'
              buttonText='subModule5.buttonText'
              image={images.sub_module_5}
            ></SubModule>
            <MainModule title='faq.title' text='faq.text' noLogos>
              <FAQContent />
            </MainModule>
          </Layout>
        </div>
      </ThemeProvider>
    );
  }
}

export default App;
