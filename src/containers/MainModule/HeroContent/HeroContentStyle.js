import styled from "styled-components";

import { device } from "../../../GlobalStyles";

export const SmallText = styled.div`
  display: flex;
  padding: 70px 0;
  opacity: 0;

  @media ${device.aboveTablet} {
    padding: 50px 0 0 0;
    color: #656565;
    opacity: 1;
    font-size: 0.875em;
  }
`;

export const SmallTextBold = styled.span`
  color: #191919;
  padding-left: 5px;
  font-weight: 700;
`;

export const PlatformImage = styled.img`
  object-fit: contain;
  width: 107%;
  height: 100%;

  @media ${device.aboveTablet} {
    width: 100%;
  }
`;
