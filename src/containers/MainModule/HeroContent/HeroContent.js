import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";

import Input from "../../../components/UI/Input/Input";

import { images } from "../../../GlobalVariables";
import { SmallText, SmallTextBold, PlatformImage } from "./HeroContentStyle";

class HeroContent extends Component {
  render() {
    return (
      <Fragment>
        <Input input="hero.input" button="hero.button" />
        <SmallText>
          <FormattedMessage id="hero.simpleLabel" />
          <SmallTextBold>
            <FormattedMessage id="hero.boldLabel" />
          </SmallTextBold>
        </SmallText>
        <PlatformImage src={images.platform} />
      </Fragment>
    );
  }
}

export default HeroContent;
