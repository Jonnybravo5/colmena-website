import styled from 'styled-components';

import { images, sizes } from '../../GlobalVariables';
import { device } from '../../GlobalStyles';

export const MainModuleContainer = styled.section`
  display: flex;
  flex-direction: column;
  width: 100%;

  padding: 0 20px 25px;
  box-sizing: border-box;

  background-image: url(${props =>
    props.background && props.background.mobile});
  background-size: contain;
  background-repeat: no-repeat;
  background-position-y: 220px;

  @media ${device.aboveTablet} {
    background-image: url(${props =>
      props.background && props.background.desktop});
    background-position-y: 0;

    padding: 80px 0 150px 0;
    box-sizing: border-box;
  }
`;

export const TitleSubtitleContainer = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  padding-top: 10px;
  padding-bottom: 15px;

  @media ${device.aboveTablet} {
    padding: 0
      calc(
        ${sizes.header.sidePadding} + ${sizes.header.navigationPaddingLeft} +
          ${sizes.header.logoWidth}
      );
  }
`;

export const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  @media ${device.aboveTablet} {
    padding: 55px calc(${sizes.header.sidePadding} - 50px) 0
      calc(${sizes.header.sidePadding} - 50px);
    box-sizing: border-box;
  }
`;

export const LogosContainer = styled.div`
  display: ${props => (props.hidden ? 'none' : 'grid')}
  grid-template-columns: repeat(2, 1fr);
  row-gap: 25px;
  column-gap: 30px;

  padding: 0;
  box-sizing: border-box;

  @media ${device.aboveTablet} {
    grid-template-columns: repeat(auto-fit, minmax(120px, 1fr));
    padding: 0 calc(${sizes.header.sidePadding} - 50px);
    height: 40px;
  }
`;

export const LogoWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 15px 0;
  box-sizing: border-box;

  @media ${device.aboveTablet} {
    padding: 0;
  }
`;

export const Logo = styled.img`
  object-fit: contain;
  max-width: 120px;
`;
