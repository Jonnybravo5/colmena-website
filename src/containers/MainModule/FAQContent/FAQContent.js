import React, { Component, Fragment } from 'react';

import {
  FAQWrapper,
  Accordion,
  AccordionHeader,
  AccordionContent,
  Chevron
} from './FAQContentStyle';

import langTranslations from './../../../static_files/langs/langs';
import { icons } from '../../../GlobalVariables';

class FAQContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      openQuestionIndex: -1
    };
  }

  openQuestion = accordionIdx => event => {
    this.setState({
      openQuestionIndex:
        this.state.openQuestionIndex !== accordionIdx ? accordionIdx : -1
    });
  };

  render() {
    const language = 'pt';
    const questions = langTranslations[language]['faq.questions'];

    return (
      <Fragment>
        <FAQWrapper>
          {questions.map((elem, i) => (
            <Accordion onClick={this.openQuestion(i)}>
              <AccordionHeader>
                {elem.title}
                <Chevron
                  src={
                    this.state.openQuestionIndex !== i
                      ? icons.chevron_close
                      : icons.chevron_open
                  }
                ></Chevron>
              </AccordionHeader>
              <AccordionContent hidden={this.state.openQuestionIndex !== i}>
                {elem.content}
              </AccordionContent>
            </Accordion>
          ))}
        </FAQWrapper>
      </Fragment>
    );
  }
}

export default FAQContent;
