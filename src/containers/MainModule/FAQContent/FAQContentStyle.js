import styled from 'styled-components';

import { device } from '../../../GlobalStyles';
import { sizes } from '../../../GlobalVariables';

export const FAQWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  @media ${device.aboveTablet} {
    padding: 0 calc(${sizes.faq.sidePadding} - 85px);
  }
`;

export const Accordion = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;

  cursor: pointer;
  border-radius: 8px;
  background-color: #fbfbfb;
  margin-bottom: 8px;
`;

export const AccordionHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  font-size: 2.8em;
  font-weight: 900;

  padding: 35px;
  box-sizing: border-box;

  @media ${device.aboveTablet} {
    padding: 40px 120px;
    font-size: 1.875em;
    line-height: 1.07;
  }
`;

export const AccordionContent = styled.div`
  display: ${props => (props.hidden ? 'none' : 'flex')};

  font-size: 2.2em;
  line-height: 1.64;

  padding: 0 35px 35px 35px;
  box-sizing: border-box;

  @media ${device.aboveTablet} {
    padding: 0 240px 45px 120px;
    font-size: 1.625em;
    line-height: 1.38;
  }
`;

export const Chevron = styled.img`
  width: 2rem;
  height: 2rem;
  object-fit: contain;
`;
