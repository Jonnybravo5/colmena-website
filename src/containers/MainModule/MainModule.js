import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';

import { images } from '../../GlobalVariables';

import {
  MainModuleContainer,
  TitleSubtitleContainer,
  ContentContainer,
  LogosContainer,
  LogoWrapper,
  Logo
} from './MainModuleStyle';

class MainModule extends Component {
  render() {
    console.log(this.props.background);
    return (
      <MainModuleContainer {...this.props}>
        <TitleSubtitleContainer>
          <h1>
            <FormattedMessage id={this.props.title} />
          </h1>
          <h3>
            <FormattedMessage id={this.props.text} />
          </h3>
        </TitleSubtitleContainer>
        <ContentContainer>{this.props.children}</ContentContainer>
        <LogosContainer hidden={this.props.noLogos}>
          <LogoWrapper>
            <Logo src={images.logo1} />
          </LogoWrapper>
          <LogoWrapper>
            <Logo src={images.logo2} />
          </LogoWrapper>
          <LogoWrapper>
            <Logo src={images.logo3} />
          </LogoWrapper>
          <LogoWrapper>
            <Logo src={images.logo4} />
          </LogoWrapper>
          <LogoWrapper>
            <Logo src={images.logo5} />
          </LogoWrapper>
        </LogosContainer>
      </MainModuleContainer>
    );
  }
}

export default MainModule;
