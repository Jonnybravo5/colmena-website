import styled from 'styled-components';

import { device } from '../../../GlobalStyles';

export const PlanContainer = styled.div`
  display: flex;
  flex-direction: column;

  @media ${device.aboveTablet} {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    column-gap: 30px;
    padding-bottom: 50px;
    box-sizing: border-box;
  }
`;
