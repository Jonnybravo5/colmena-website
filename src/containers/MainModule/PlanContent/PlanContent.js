import React, { Component, Fragment } from 'react';

import Input from '../../../components/UI/Input/Input';

import { PlanContainer } from './PlanContentStyle';
import Plan from '../../../components/Plan/Plan';

class PlanContent extends Component {
  render() {
    return (
      <PlanContainer>
        <Plan background='white'></Plan>
        <Plan background='purple'></Plan>
      </PlanContainer>
    );
  }
}

export default PlanContent;
