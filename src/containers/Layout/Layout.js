import React, { Component, Fragment } from 'react';

import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';

import { Main } from './LayoutStyle';

class Layout extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showSideDrawer: false
    };
  }

  sideDrawerClosedHandler = () => {
    this.setState({ showSideDrawer: false });
  };

  sideDrawerToggleHandler = () => {
    this.setState(prevState => {
      return { showSideDrawer: !prevState.showSideDrawer };
    });
  };

  render() {
    return (
      <Fragment>
        <Header
          drawerToggleClicked={this.sideDrawerToggleHandler}
          open={this.state.showSideDrawer}
          closed={this.sideDrawerClosedHandler}
        />
        <Main>{this.props.children}</Main>
        <Footer />
      </Fragment>
    );
  }
}

export default Layout;
