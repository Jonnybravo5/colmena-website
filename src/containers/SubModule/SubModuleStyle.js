import styled, { css } from 'styled-components';

import { images, sizes } from '../../GlobalVariables';
import { device } from '../../GlobalStyles';

export const SubModuleContainer = styled.section`
  display: flex;
  flex-direction: column;
  padding: 30px 0;
  box-sizing: border-box;

  background-color: ${props =>
    props.background === 'white'
      ? props.theme.background
      : props.theme.backgroundGrey};

  @media ${device.aboveTablet} {
    flex-direction: ${props =>
      props.imagePos === 'right' ? 'row-reverse' : 'row'};
    padding: 140px 0;
  }
`;

export const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 100%;

  padding: 0 20px;
  box-sizing: border-box;

  @media ${device.aboveTablet} {
    padding: 0 135px 0 135px;
    width: 50%;
  }
`;

export const Category = styled.span`
  color: #ff6600;
  font-family: Catamaran;
  font-size: 1.6em;
  font-weight: 900;

  @media ${device.aboveTablet} {
    font-size: 1.25em;
  }
`;

export const ImageContainer = styled.div`
  display: flex;
  width: 100%;
  padding-top: 30px;

  ${props =>
    props.imagePos === 'right'
      ? css`
          padding-right: 20px;
        `
      : css`
          padding-left: 20px;
        `};

  box-sizing: border-box;

  @media ${device.aboveTablet} {
    width: 50%;
    padding: 0;
  }
`;

export const Image = styled.img`
  object-fit: contain;
  width: 100%;
  height: 100%;
`;

export const TextWrapper = styled.div`
  font-size: 2.2em;
  line-height: 1.64;
  padding-bottom: 30px;
  box-sizing: border-box;

  @media ${device.aboveTablet} {
    line-height: 1.38;
    font-size: 1.525em;
  }
`;

export const ButtonWrapper = styled.div`
  display: ${props => (props.hidden ? 'none' : 'flex')};

  button {
    width: 50%;
  }
`;
