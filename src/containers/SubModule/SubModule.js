import React, { Component, Fragment } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';

import { Button } from '../../components/UI/Button/Button';

import {
  SubModuleContainer,
  TextContainer,
  Category,
  ImageContainer,
  Image,
  TextWrapper,
  ButtonWrapper
} from './SubModuleStyle';

class SubModule extends Component {
  constructor(props) {
    super(props);

    console.log(!props.intl.messages[props.buttonText]);
  }

  render() {
    return (
      <SubModuleContainer {...this.props}>
        <TextContainer>
          <Category>
            <FormattedMessage id={this.props.category} />
          </Category>
          <h2>
            <FormattedMessage id={this.props.title} />
          </h2>
          <TextWrapper>
            <FormattedMessage id={this.props.text} />
          </TextWrapper>
          <ButtonWrapper
            hidden={!this.props.intl.messages[this.props.buttonText]}
          >
            <Button big>
              <FormattedMessage id={this.props.buttonText} />
            </Button>
          </ButtonWrapper>
        </TextContainer>
        <ImageContainer imagePos={this.props.imagePos}>
          <Image src={this.props.image} />
        </ImageContainer>
      </SubModuleContainer>
    );
  }
}

export default injectIntl(SubModule);
