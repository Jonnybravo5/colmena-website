import translation_pt from "./pt.json";
import translation_en from "./en.json";

const langTranslations = {
    'pt': translation_pt,
    'en': translation_en
};

export default langTranslations;