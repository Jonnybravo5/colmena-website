import styled from 'styled-components';
import { device } from '../../GlobalStyles';
import { sizes } from '../../GlobalVariables';

export const FooterStyle = styled.footer`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${props => props.theme.background};

  color: white;
  border: solid 1px #f5f5f5;

  @media ${device.aboveTablet} {
    height: ${sizes.footer.height};
  }
`;

export const FooterWrapper = styled.div`
  display: flex;
  flex-direction: column-reverse;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 100%;
  padding: 0 20px;
  box-sizing: border-box;

  @media ${device.aboveTablet} {
    flex-direction: row;
    padding: 0 ${sizes.footer.sidePadding};
  }
`;

export const Navigation = styled.nav`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;

  @media ${device.aboveTablet} {
    justify-content: unset;
    width: auto;
  }
`;

export const NavigationItems = styled.div`
  display: flex;
  flex-grow: 1;

  flex-direction: row;
  width: 100%;
  justify-content: space-between;

  padding-bottom: 30px;
  box-sizing: border-box;

  @media ${device.aboveTablet} {
    display: grid;
    grid-template-columns: repeat(3, fit-content(100%));
    column-gap: 50px;

    padding-bottom: 0;
  }
`;

export const NavigationItem = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  padding: 0;
  box-sizing: border-box;

  @media ${device.aboveTablet} {
    justify-content: flex-end;
    padding: 35px 0;
  }
`;

export const Anchor = styled.span`
  display: block;
  font-family: 'Catamaran', sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 2;
  text-align: center;
  text-decoration: none;
  color: ${props => props.theme.grey};
  cursor: pointer;
  height: 28px;

  &:hover {
    color: ${props => props.theme.primary};
  }
`;

export const LogoSocialNetworksWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;

  padding: 20px 0;
  box-sizing: border-box;

  @media ${device.aboveTablet} {
    width: calc(50% + 60px); //logo width/2
    padding: 0;
  }
`;

export const Logo = styled.div`
  display: flex;
  align-items: center;
  max-height: 30px;
  width: 50%;

  @media ${device.aboveTablet} {
    flex-grow: 2;
  }
`;

export const LogoImage = styled.img`
  height: 100%;
  width: ${sizes.footer.logoWidth};
  object-fit: contain;
`;

export const SocialNetwork = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  width: 50%;
  justify-content: flex-end;

  @media ${device.aboveTablet} {
    width: 100%;
  }
`;

export const SocialNavigationItems = styled.div`
  display: grid;
  grid-template-columns: repeat(3, minmax(20px, 1fr));
  column-gap: 30px;

  @media ${device.aboveTablet} {
    grid-template-columns: repeat(3, fit-content(100%));
  }
`;

export const SocialIcon = styled.img`
  height: 100%;
  width: 100%;
  object-fit: contain;
  height: 15px;
`;

export const SocialLink = styled.span`
  display: block;
  font-family: 'Catamaran', sans-serif;
  font-style: normal;
  font-weight: 900;
  font-size: 18px;
  line-height: 1;
  text-align: center;
  -webkit-text-decoration: none;
  text-decoration: none;
  color: #afafaf;
  cursor: pointer;
  height: 17px;

  &:hover {
    color: ${props => props.theme.primary};
  }
`;
