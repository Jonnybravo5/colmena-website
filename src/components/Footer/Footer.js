import React, { Component, Fragment } from 'react';
import { FormattedMessage } from 'react-intl';

import {
  FooterStyle,
  FooterWrapper,
  Navigation,
  NavigationItems,
  NavigationItem,
  Anchor,
  LogoSocialNetworksWrapper,
  SocialNetwork,
  SocialNavigationItems,
  SocialIcon,
  SocialLink,
  Logo,
  LogoImage
} from './FooterStyle';

import { images, icons } from '../../GlobalVariables';

class Footer extends Component {
  render() {
    const footerlinks = [
      'footer.colmena',
      'footer.privacy',
      'footer.termsAndConditions'
    ];

    const navigationItems = (
      <Fragment>
        <NavigationItems>
          {footerlinks.map((elem, i) => (
            <NavigationItem>
              <Anchor>
                <FormattedMessage id={elem} />
              </Anchor>
            </NavigationItem>
          ))}
        </NavigationItems>
      </Fragment>
    );

    const socialItems = (
      <Fragment>
        <SocialNavigationItems>
          <NavigationItem>
            <SocialIcon src={icons.social_facebook} alt='facebook' />
          </NavigationItem>
          <NavigationItem>
            <SocialLink>in</SocialLink>
          </NavigationItem>
          <NavigationItem>
            <SocialIcon src={icons.social_twitter} alt='twitter' />
          </NavigationItem>
        </SocialNavigationItems>
      </Fragment>
    );

    return (
      <FooterStyle>
        <FooterWrapper>
          <Navigation>{navigationItems}</Navigation>
          <LogoSocialNetworksWrapper>
            <Logo>
              <LogoImage src={images.logo_footer} alt='logo' />
            </Logo>
            <SocialNetwork>{socialItems}</SocialNetwork>
          </LogoSocialNetworksWrapper>
        </FooterWrapper>
      </FooterStyle>
    );
  }
}

export default Footer;
