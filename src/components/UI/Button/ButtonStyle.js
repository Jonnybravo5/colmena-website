import { css } from 'styled-components';
import { device } from '../../../GlobalStyles';

export const ButtonStyle = css`
  display: flex;
  justify-content: center;
  align-items: center;
  border: none;
  outline: none;
  background: ${props => props.theme.primary};
  border-radius: 4px;
  padding: 10px 20px;
  box-sizing: border-box;
  cursor: pointer;
  
  color: white;
  font-family: 'Catamaran';
  font-weight: 700;
  font-size: 1.600em;
  text-transform: uppercase;

  // min-width: 175px;
  // max-width: 250px;
  max-height: 55px;
  min-height: 40px;
  // width: 100%;
  // align-self: center;

  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;

  @media ${device.aboveTablet} {
    font-size: 1em;
  }

  ${props =>
    props.inverted &&
    css`
      background: white;
      color: ${props => props.theme.primary};
    `}
  
  ${props =>
    props.border === 'white' &&
    css`
      border: 1px solid #ffffff;
      font-weight: 900;
      color: #ffffff;
      background: transparent;
    `}
  
  ${props =>
    props.border === 'grey' &&
    css`
      border: 1px solid #f0e7ff;
      text-transform: none;
      font-weight: 600;
    `}

  ${props =>
    props.border === 'black' &&
    css`
      border: 1px solid #000000;
      font-weight: 900;
      color: #000000;
      background: transparent;
    `}

  ${props =>
    props.big &&
    css`
      height: 54px;
    `}
`;
