import styled from 'styled-components';

import { ButtonStyle } from './ButtonStyle';

/**
 * Has the general style for the website button.
 */
export const Button = styled.button`
  ${ButtonStyle}
`;
