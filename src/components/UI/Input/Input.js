import React, { Component, Fragment } from 'react';
import { FormattedMessage } from "react-intl";
import { injectIntl, intlShape } from 'react-intl';

import { Button } from '../Button/Button';

import { InputWrapper, InputStyle } from './InputStyle';

/**
 * Defines the general style for inputs in the application.
 */
class Input extends Component {
  render() {
    return (
      <InputWrapper {...this.props}>
        <InputStyle placeholder={this.props.intl.formatMessage({id: this.props.input})}></InputStyle>
        <Button big>
        <FormattedMessage id={this.props.button} />
        </Button>
      </InputWrapper>
    );
  }
}

export default injectIntl(Input);
