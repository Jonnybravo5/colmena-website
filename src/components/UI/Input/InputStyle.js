import styled, { css } from 'styled-components';
import { device } from '../../../GlobalStyles';

export const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-self: center;
  width: 100%;

  @media ${device.aboveTablet} {
    position: relative;
    width: auto;

    button {
      position: absolute;
      right: 7px;
      top: 7px;
      height: 40px;
    }
  }
`;

export const InputStyle = styled.input`
  height: 54px;

  border: 1px solid #f0e7ff;
  border-radius: 4px;
  padding: 20px;
  box-sizing: border-box;
  margin-bottom: 10px;

  font-family: 'Catamaran', sans-serif;
  font-size: 1.6em;
  font-weight: 700;
  text-align: center;

  background: #ffffff;
  color: ${props => props.theme.primary};

  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;

  ::placeholder {
    color: ${props => props.theme.primary};
    opacity: 1;
  }

  @media ${device.aboveTablet} {
    min-width: 480px;
    min-height: 55px;

    padding: 5px 230px 5px 20px;
    margin-bottom: 0;

    font-size: 1em;
    text-align: left;
  }
`;
