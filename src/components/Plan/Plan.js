import React, { Component, Fragment } from 'react';

import { Button } from '../../components/UI/Button/Button';

import {
  PlanContainer,
  TypeWrapper,
  Type,
  SubType,
  PriceWrapper,
  Price,
  PriceDescription,
  Description,
  ItemContainer,
  Item,
  ButtonWrapper
} from './PlanStyle';

class Plan extends Component {
  render() {
    return (
      <PlanContainer background={this.props.background}>
        <TypeWrapper>
          <Type>Plano Mensal</Type>
          <SubType>A melhor opção!</SubType>
        </TypeWrapper>
        <PriceWrapper background={this.props.background}>
          <Price>5€</Price>
          <PriceDescription>por colaborador</PriceDescription>
        </PriceWrapper>
        <Description>Tudo incluído</Description>
        <ItemContainer>
          <Item background={this.props.background}>
            Download Free Song For Ipod
          </Item>
          <Item background={this.props.background}>
            Download Free Song For Ipod
          </Item>
          <Item background={this.props.background}>
            Download Free Song For Ipod
          </Item>
          <Item background={this.props.background}>
            Download Free Song For Ipod
          </Item>
          <Item background={this.props.background}>
            Download Free Song For Ipod
          </Item>
        </ItemContainer>
        <ButtonWrapper>
          <Button
            big
            inverted
            border={this.props.background === 'white' ? 'black' : 'white'}
          >
            Subscrever agora
          </Button>
        </ButtonWrapper>
      </PlanContainer>
    );
  }
}

export default Plan;
