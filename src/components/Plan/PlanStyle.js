import styled, { css } from 'styled-components';

import { icons } from '../../GlobalVariables';
import { device } from '../../GlobalStyles';

export const PlanContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${props =>
    props.background === 'white'
      ? props.theme.background
      : props.theme.primary};
  color: ${props =>
    props.background === 'white' ? css`#000000` : css`#ffffff`};
  width: 100%;
  border-radius: 8px;

  padding: 30px 60px;
  margin-bottom: 30px;
  box-shadow: 0px 2px 34px rgba(121, 121, 121, 0.5);
  box-sizing: border-box;

  @media ${device.aboveTablet} {
    max-width: 350px;
  }
`;

export const TypeWrapper = styled.div`
  display: flex;
  align-items: center;

  @media ${device.aboveTablet} {
  }
`;

export const Type = styled.div`
  display: flex;
  flex-direction: column;

  padding: 5px 0;
  box-sizing: border-box;

  font-size: 1.8em;

  @media ${device.aboveTablet} {
    font-size: 1.125em;
  }
`;

export const SubType = styled.div`
  padding-left: 5px;
  font-size: 1.2em;
  color: #29c3a9;

  @media ${device.aboveTablet} {
    font-size: 0.75em;
  }
`;

export const PriceWrapper = styled.div`
  display: flex;
  flex-direction: column;

  width: 100%;
  background-color: ${props =>
    props.background === 'white' ? css`#f5f5f5` : css`#7b25fe`};
  align-items: center;

  padding: 10px 0 5px;
  box-sizing: border-box;

  @media ${device.aboveTablet} {
  }
`;

export const Price = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 5em;
  font-weight: 900;
  line-height: 1;

  @media ${device.aboveTablet} {
    font-size: 3.125em;
  }
`;

export const PriceDescription = styled.div`
  display: flex;
  font-size: 1.4em;

  @media ${device.aboveTablet} {
    font-size: 0.875em;
  }
`;

export const Description = styled.div`
  display: flex;
  flex-direction: column;

  font-size: 2.2em;
  font-weight: 700;

  padding-top: 25px;
  box-sizing: border-box;

  @media ${device.aboveTablet} {
    font-size: 1.375em;
  }
`;
export const ItemContainer = styled.div`
  display: flex;
  flex-direction: column;

  padding: 10px 0 25px;
  box-sizing: border-box;

  width: 100%;

  @media ${device.aboveTablet} {
  }
`;
export const Item = styled.div`
  display: flex;
  align-items: flex-start;

  font-size: 1.6em;
  color: ${props =>
    props.background === 'white'
      ? css`rgba(0, 0, 0, 0.75)`
      : css`rgba(255, 255, 255, 0.75)`};

  padding: 5px 0;
  box-sizing: border-box;

  &:before {
    content: url(${props =>
      props.background === 'white' ? icons.included : icons.included_white});
    padding-right: 12px;
  }

  @media ${device.aboveTablet} {
    font-size: 1em;
  }
`;
export const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;

  width: 100%;

  @media ${device.aboveTablet} {
  }
`;
