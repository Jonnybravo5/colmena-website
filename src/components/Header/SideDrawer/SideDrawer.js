import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
// import { NavLink } from 'react-router-dom';

import {
  SideDrawer,
  Backdrop,
  SideDrawerHeader,
  Logo,
  LogoImage,
  CloseBtn
} from './SideDrawerStyle';

import closeBtn from '../../../assets/images/close-button.svg';

/**
 *  It has a first section that has the logo and a close button.
 *  The middle section has the navigation items that are also present on the navigation bar.
 *  On the bottom there is a button for the ContactPage.
 */
const sideDrawer = props => {
  return (
    <Fragment>
      <Backdrop show={props.open} onClick={props.closed} />
      <SideDrawer {...props}>
        <SideDrawerHeader>
          <Logo>
            {/* <NavLink to='/'>
              <LogoImage
                src={'https://images.ianum.com/logo/logo.png'}
                alt='ianum_logo'
              />
            </NavLink> */}
          </Logo>
          <CloseBtn src={closeBtn} alt='close' onClick={props.closed} />
        </SideDrawerHeader>
        {props.children}
      </SideDrawer>
    </Fragment>
  );
};

sideDrawer.propTypes = {
  open: PropTypes.bool.isRequired,
  closed: PropTypes.func.isRequired
};

// Styled-components PropTypes
SideDrawer.propTypes = {
  open: PropTypes.bool
};

Backdrop.propTypes = {
  show: PropTypes.bool
};

export default sideDrawer;
