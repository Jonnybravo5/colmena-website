import styled from 'styled-components';
import { device } from '../../../GlobalStyles';

export const SideDrawer = styled.div`
  position: fixed;
  display: flex;
  flex-direction: column;
  width: 350px;
  max-width: 70%;
  height: 100%;
  right: 0;
  top: 0;
  z-index: 10;
  background-color: ${props => props.theme.background};
  padding: 32px;
  box-sizing: border-box;
  transition: transform 0.4s ease-out;
  transform: ${props => (props.open ? 'translateX(0)' : 'translateX(100%)')};

  @media ${device.aboveTablet} {
    display: none;
  }
`;

export const Backdrop = styled.div`
  display: ${props => (props.show ? 'flex' : 'none')};
  width: 100%;
  height: 100%;
  position: fixed;
  z-index: 2;
  left: 0;
  top: 0;
  background-color: rgba(0, 0, 0, 0.3);
`;

export const SideDrawerHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const Logo = styled.div`
  max-height: 50px;
  max-width: 125px;
  cursor: pointer;
`;

export const LogoImage = styled.img`
  height: 100%;
  width: 100%;
  object-fit: contain;
`;

export const CloseBtn = styled.img`
  height: 20px;
  width: 20px;
  object-fit: contain;
  cursor: pointer;
`;
