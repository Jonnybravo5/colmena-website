import styled from 'styled-components';
import { device } from '../../GlobalStyles';
import { sizes } from '../../GlobalVariables';

export const HeaderStyle = styled.header`
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${props => props.theme.background};
  height: ${sizes.header.height};
  color: white;
  border-bottom: solid 1px #f3f3f3;
`;

export const HeaderWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 100%;
  padding: 0 20px;
  box-sizing: border-box;

  @media ${device.aboveTablet} {
    padding: 0 ${sizes.header.sidePadding};
  }
`;

export const Navigation = styled.nav`
  display: none;

  @media ${device.aboveTablet} {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    flex-grow: 1;

    padding-left: ${sizes.header.navigationPaddingLeft};
    box-sizing: border-box;
  }
`;

export const NavigationItems = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;

  @media ${device.aboveTablet} {
    display: grid;
    grid-template-columns: repeat(4, fit-content(100%));
    column-gap: 30px;
  }
`;

export const NavigationItem = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  text-align: center;
  padding: 20px 0;
  box-sizing: border-box;

  @media ${device.aboveTablet} {
    justify-content: center;
  }
`;

export const Anchor = styled.span`
  display: block;
  font-family: 'Catamaran', sans-serif;
  font-style: normal;
  font-weight: 700;
  font-size: 16px;
  line-height: 22px;
  text-align: center;
  text-decoration: none;
  color: #000000;
  cursor: pointer;

  &:hover {
    color: #40a4c8;
  }
`;

export const Logo = styled.div`
  display: flex;
  align-items: center;
  max-height: 25px;
  max-width: ${sizes.header.logoWidth};
`;

export const LogoImage = styled.img`
  height: 100%;
  width: 100%;
  object-fit: contain;
`;

export const DrawerToggle = styled.div`
  width: 40px;
  height: 100%;
  display: flex;
  flex-flow: column;
  justify-content: space - around;
  align-items: center;
  padding: 10px 0;
  box-sizing: border - box;
  cursor: pointer;
  color: black;

  @media ${device.aboveTablet} {
    display: none;
  }

  div {
    width: 90 %;
    height: 3px;
    background-color: white;
  }
`;

export const RightNav = styled.div`
  display: flex;
`;

export const ContactButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 0;

  @media ${device.aboveTablet} {
    margin-left: 25px;
  }

  a {
    width: 100%;
  }
`;
