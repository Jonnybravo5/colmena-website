import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
// import { NavLink } from 'react-router-dom';

import { Button } from '../UI/Button/Button';
import { FormattedMessage } from 'react-intl';

import {
  HeaderStyle,
  HeaderWrapper,
  Navigation,
  NavigationItems,
  RightNav,
  NavigationItem,
  Anchor,
  Logo,
  LogoImage,
  DrawerToggle,
  ContactButtonWrapper
} from './HeaderStyle';

import SideDrawer from './SideDrawer/SideDrawer';

import menu_close_icon from '../../assets/images/menu_icon.svg';
import { images } from '../../GlobalVariables';

/**
 * The header has two distinct parts, for the desktop it has the logo, a navigation bar and a button and for mobile it just has the logo and a button to toggle a SideDrawer.
 * The children used on the navigation bar and on the SideDrawer are the same.
 */
class Header extends Component {
  render() {
    const headerlinks = [
      'header.whyColmena',
      'header.resources',
      'header.aboutUs',
      'header.price'
    ];

    const navigationItems = (
      <Fragment>
        <NavigationItems>
          {headerlinks.map((elem, i) => (
            <NavigationItem>
              <Anchor>
                <FormattedMessage id={elem} />
              </Anchor>
            </NavigationItem>
          ))}
        </NavigationItems>
        <RightNav>
          <NavigationItem>
            <Anchor>
              <FormattedMessage id='header.logIn' />
            </Anchor>
          </NavigationItem>
          <ContactButtonWrapper>
            <Button inverted border='grey' big>
              <FormattedMessage id='header.tryForFree' />
            </Button>
          </ContactButtonWrapper>
        </RightNav>
      </Fragment>
    );

    return (
      <HeaderStyle>
        <HeaderWrapper>
          <Logo>
            <LogoImage src={images.logo} alt='logo' />
          </Logo>
          <Navigation>{navigationItems}</Navigation>

          <DrawerToggle onClick={this.props.drawerToggleClicked}>
            <img src={menu_close_icon} alt='MENU' />
          </DrawerToggle>
          <SideDrawer open={this.props.open} closed={this.props.closed}>
            {navigationItems}
          </SideDrawer>
        </HeaderWrapper>
      </HeaderStyle>
    );
  }
}

Header.propTypes = {
  drawerToggleClicked: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  closed: PropTypes.func.isRequired
};

export default Header;
